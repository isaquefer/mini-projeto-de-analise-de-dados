from pymongo import MongoClient
import urllib.parse

username_mongo = urllib.parse.quote_plus('root')
password = urllib.parse.quote_plus('MongoDB2019!')

client_mongo = MongoClient(f'mongodb://{username_mongo}:{password}@127.0.0.1:27017')
db_mongo = client_mongo.twitterdb
