import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

from listener import col

dataset = [{"created_at": item['created_at'], "text": item["text"]} for item in col.find()]

df = pd.DataFrame(dataset)

cv = CountVectorizer()
count_matrix = cv.fit_transform(df.text)

word_count = pd.DataFrame(cv.get_feature_names(), columns=["word"])
word_count["count"] = count_matrix.sum(axis=0).tolist()[0]
word_count = word_count.sort_values("count", ascending=False).reset_index(drop=True)
print(word_count[:50])
