from time import sleep

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler, Stream, API
from decouple import Config, RepositoryEnv

from db import db_mongo
from listener import MyListener


DOTENV_FILE = 'config.ini'
env_config = Config(RepositoryEnv(DOTENV_FILE))

consumer_key = env_config.get('consumer_key')
consumer_secret = env_config.get('consumer_secret')

token = env_config.get('token')
token_secret = env_config.get('token_secrets')

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(token, token_secret)
api = API(auth)

my_listener = MyListener()

my_stream = Stream(api.auth, listener=my_listener)

keywords = ['Big Data', 'Python', 'Data Mining', 'Data science']

try:
    print("======================================================")
    print("======================================================")
    print('--------Digite Ctrl + C para cancelar a busca---------')
    print('--------Digite Ctrl + C para cancelar a busca---------')
    print('--------Digite Ctrl + C para cancelar a busca---------')
    print("======================================================")
    print("======================================================")
    input("Pressione Enter para continuar: ")
    print("Iniciando busca...")

    my_stream.filter(track=keywords)
except KeyboardInterrupt:
    my_stream.disconnect()
