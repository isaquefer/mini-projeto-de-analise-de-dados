import json

from tweepy.streaming import StreamListener

from db import db_mongo

col = db_mongo.tweets

class MyListener(StreamListener):
    def on_error(self, status_code):
        print(status_code)
        return False

    def on_data(self, dados):
        tweet = json.loads(dados)
        created_at = tweet['created_at']
        id_str = tweet['id_str']
        text = tweet['text']
        obj = {
            "created_at": created_at,
            "id_str": id_str,
            "text": text
        }
        tweetind = col.insert_one(obj).inserted_id
        print(obj)
        return True
