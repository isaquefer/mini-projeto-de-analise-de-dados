# Mini projeto de analise de dados 

Esse projeto foi criado no curso da [datascience academy](https://www.datascienceacademy.com.br/)
de introdução a análises de dados com python com algumas modificações.

## Dependências

Sem essas dependências o projeto NÃO IRÁ FUNCIONAR!

* [make](https://www.gnu.org/software/make/manual/make.html)
* [python](https://www.python.org/)
* [pip](https://pip.pypa.io/en/stable/)
* [git](https://git-scm.com/)
* [docker-compose](https://docs.docker.com/compose/)

Será necessário uma conta de desenvolvedor no twitter para fazer a coleta
de tweets, depois de criada a conta crie um app e pegue os tokens fornecido
pelo app para fazer as requisições.

* [como criar a conta](https://medium.com/@marlessonsantana/como-criar-apps-e-obter-os-tokens-necess%C3%A1rios-para-coletar-dados-do-twitter-instagram-linkedin-e-8f36602ea92a)
* [twitter](https://developer.twitter.com)

## Sobre
Esse mini projeto pega tweets que tenham as palavras Big data, python, Data Mining,
Data sciente, e salva em um banco no MongoDB e retornam as palavras que mais aparecem
nos tweets com essas palavras chaves.

## Commands

* `make` - Instalando projeto e criando arquivo de configuração.
* `pipenv shell` - Ativando o ambiente isolado do projeto
* `make run` - Inicializar a busca por tweets.
* `make analise` - Retorna as palavras que mais aparecem nos tweets.

