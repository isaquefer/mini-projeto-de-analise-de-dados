#!/bin/zsh

###############
#
#  Script para configurar o ambiente do projeto
#  Dependências
#  Make, pip, git
#
###############

pip install pipenv
pipenv install

PATH_ROOT=$(pwd)"/MongoDB"

if [ ! -e $PATH_ROOT ]; then
    $(mkdir $PATH_ROOT)
fi

docker-compose up -d

python create_configuration.py
