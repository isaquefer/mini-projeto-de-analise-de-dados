from os import remove
from os.path import exists

if exists('config.ini'):
    remove('config.ini')


key_words = {
    'twitter': ['consumer_secret', 'consumer_key', 'token', 'token_secrets']
}
section_text = ''.join([key_word for key_word in key_words.keys() if key_word == 'twitter'])
section_text_formated = ''.join(('[', section_text, ']'))


with open('config.ini', 'w') as file:
    file.write(section_text_formated)

    for key in key_words:
        for value in key_words[key]:
            text = input(f"Digite o {value}: ")
            file.write(f'\n{value} = {text}')
